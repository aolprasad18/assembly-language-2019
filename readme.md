# Build Commands:-

## Preprocess :
 **cpp hello.c > hello.c**
 `cpp is c preprocessor @ /usr/bin/cpp`

## Compile :
 **cc1 -o hello.S hello.c**
 `cc1 is c compiler @ /usr/lib/gcc/i686-linux-gnu/5.4.0/cc1. 5.4.0 is compiler version. i686 refers to 32 bit.`

## Assemble :
 **as -o hello.o hello.S**
 `as is assembler @ /usr/bin/as`
 `This is a GNU Assembler also called as GAS. This is invoked by gcc internally.`

## Link :
 **ld -o hello -lc -dynamic-linker /lib/ld-linux.so.2 hello.o -e main**
 `ld is static loader @ /usr/bin/ld`
 `-lc is standalone argument to statically link libc i.e. lc, 'lib' is common start to library and hence can be skipped`
 `-dynamic-linker /lib/ld-linux.so.2` is optarg i.e. option to argument which is a dynamic linker present as a shared object. ld-linux.so.2 is a soft link`
 `-e main is entry point function`
